import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import libraryImage from "./libraryImage.jpg";
class HomePage extends Component {
  state = {
    search: "",
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    s1[input.name] = input.value;
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let json = {};
    json.searchText = this.state.search;
    json.startIndex = 0;
    json.maxResults = 8;
    this.callURL("/books", json);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { searchText, startIndex, maxResults } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "searchText", searchText);
    searchStr = this.addToQueryString(searchStr, "startIndex", startIndex);
    searchStr = this.addToQueryString(searchStr, "maxResults", maxResults);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue === 0 || paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { search = "" } = this.state;
    return (
      <div className="container">
        <center>
          <img src={libraryImage} className="my-3 rounded-circle shadow" />
        </center>
        <div className="row mt-5">
          <div className="col-7 offset-2">
            <div className="form-group">
              <input
                type="text"
                className="form-control mx-2"
                name="search"
                value={search}
                placeholder="Search"
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className="col-3">
            <button
              className="btn btn-primary shadow"
              onClick={this.handleSubmit}
            >
              Search
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default HomePage;
