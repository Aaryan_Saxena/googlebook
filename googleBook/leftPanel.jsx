import React, { Component } from "react";
class LeftPanel extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    options[input.name] = input.value;
    this.props.onOptionChange(options);
  };
  makeRadio = (arr, values, name, label) => {
    return (
      <div className="row">
        <div className="col-12 px-3 py-1 bg-light border">
          <label className="form-check-label font-weight-bold">{label}</label>
        </div>
        <div className="col-12 p-0">
          <div className="row">
            {arr.map((opt, index) => (
              <div className="col-12" key={opt.display}>
                <div className="form-check border">
                  <input
                    type="radio"
                    className="form-check-input mx-2"
                    value={opt.value}
                    name={name}
                    checked={values === opt.value}
                    onChange={this.handleChange}
                  />
                  <label className="form-check-label mx-4">{opt.display}</label>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  };
  makeDD = (arr, name, value, label, valOnTop) => {
    return (
      <div className="row">
        <div className="col-12 px-4 py-2 bg-light border font-weight-bold">
          <label>{label}</label>
        </div>
        <div className="col-12 p-0 ">
          <select
            className="form-control"
            name={name}
            value={value}
            onChange={this.handleChange}
          >
            <option value="">{valOnTop}</option>
            {arr.map((opt, index) => (
              <option key={index}>{opt}</option>
            ))}
          </select>
        </div>
      </div>
    );
  };
  render() {
    let {
      orderBy = "",
      langRestrict = "",
      filter = "",
      printType = "",
    } = this.props.options;
    let { orderByOpt, languages, filterOpt, printTypeOpt } = this.props;
    return (
      <div className="row mt-4 mx-2">
        {languages.length === 0 ? (
          ""
        ) : (
          <div className="col-12">
            {this.makeRadio(
              languages,
              langRestrict,
              "langRestrict",
              "Languague"
            )}
          </div>
        )}
        {filterOpt.length === 0 ? (
          ""
        ) : (
          <div className="col-12">
            <hr />
            {this.makeRadio(filterOpt, filter, "filter", "Filter")}
          </div>
        )}
        {printTypeOpt.length === 0 ? (
          ""
        ) : (
          <div className="col-12">
            <hr />
            {this.makeRadio(printTypeOpt, printType, "printType", "Print Type")}
          </div>
        )}
        {orderByOpt.length === 0 ? (
          ""
        ) : (
          <div className="col-12">
            <hr />
            {this.makeDD(
              orderByOpt,
              "orderBy",
              orderBy,
              "Order By",
              "Order By"
            )}
          </div>
        )}
      </div>
    );
  }
}
export default LeftPanel;
