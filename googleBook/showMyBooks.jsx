import React, { Component } from "react";
import { Link } from "react-router-dom";
class ShowMyBooks extends Component {
  render() {
    let { removeBook, myBooks } = this.props;
    return (
      <React.Fragment>
        {myBooks.length === 0 ? (
          <h5 className="bg-info text-warning text-center p-2">
            No Book Added to MyBooks
          </h5>
        ) : (
          <React.Fragment>
            <h5 className="bg-info text-warning text-center p-2 m-0">
              My Books List
            </h5>
            <div className="container">
              <div className="row m-0">
                {myBooks.map((bk, index) => {
                  let {
                    imageLinks = {},
                    title,
                    authors = [],
                    categories = [],
                  } = bk.volumeInfo;
                  let { smallThumbnail, thumbnail } = imageLinks;
                  return (
                    <div
                      key={bk.id}
                      className="col-3 text-center border bg-success"
                    >
                      <img src={smallThumbnail ? smallThumbnail : thumbnail} />{" "}
                      <br />
                      <h5>{title}</h5>
                      <p>
                        {authors.join(" ")}
                        <br />
                        {categories[0]}
                      </p>
                      <button
                        className="btn btn-secondary m-1"
                        onClick={() => removeBook(bk)}
                      >
                        Remove From MyBooks
                      </button>
                    </div>
                  );
                })}
              </div>
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}
export default ShowMyBooks;
