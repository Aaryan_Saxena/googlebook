import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
class Settings extends Component {
  state = {
    options: {
      filter: this.props.filterArr,
      max: this.props.max,
    },
    filterArr: [
      {
        display: "printType--(Restrict to books or magazines.)",
        value: "printType",
      },
      {
        display:
          "languages--(Restrict the volumes returned to those are tagged with the specified language.)",
        value: "languages",
      },
      {
        display:
          "filter--(Filter search results by volume type and availability.)",
        value: "filter",
      },
      {
        display: "orderBy--(Order of the volume search results.)",
        value: "orderBy",
      },
    ],
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.type === "checkbox"
      ? (s1.options[input.name] = this.updateCBs(
          s1.options[input.name],
          input.checked,
          input.value
        ))
      : (s1.options[input.name] = input.value);
    this.setState(s1);
    this.props.onOptionChange(s1.options);
  };
  updateCBs = (arr, checked, value) => {
    if (checked) arr.push(value);
    else {
      let index = arr.findIndex((val) => val === value);
      if (index >= 0) arr.splice(index, 1);
    }
    return arr;
  };
  makeCBs = (arr, values, name, label) => {
    return (
      <React.Fragment>
        <h4 className="form-check-label text-danger mt-4">{label}</h4>
        {arr.map((opt, index) => (
          <div className="form-check mx-4 my-2" key={opt.value}>
            <input
              type="checkbox"
              className="form-check-input"
              value={opt.value}
              name={name}
              checked={values.find((val) => val === opt.value) || false}
              onChange={this.handleChange}
            />
            <label className="form-check-label">{opt.display}</label>
          </div>
        ))}
      </React.Fragment>
    );
  };
  render() {
    let { options, filterArr } = this.state;
    let { filter, max } = options;
    return (
      <div className="container">
        {this.makeCBs(
          filterArr,
          filter,
          "filter",
          " Select Options for Filtering on Left Panel"
        )}
        <h5 className="text-success">No of entries on a page</h5>
        <div className="form-group">
          <input
            type="text"
            className="form-control"
            name="max"
            value={max}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}
export default Settings;
