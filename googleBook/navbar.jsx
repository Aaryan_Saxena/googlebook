import React, { Component } from "react";
import { Link } from "react-router-dom";
class Navbar extends Component {
  render() {
    let max = this.props.maxResults ? this.props.maxResults : 8;
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand text-dark">
          <i className="fas fa-book-open fa-2x"></i>
        </Link>
        <div id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link
                to={`/books?searchText=Harry Potter&startIndex=0&maxResults=${max}`}
                className="nav-link text-dark"
              >
                Harry Potter
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to={`/books?searchText=Agatha Christie&startIndex=0&maxResults=${max}`}
                className="nav-link text-dark"
              >
                Agatha Christie
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to={`/books?searchText=Premchand&startIndex=0&maxResults=${max}`}
                className="nav-link text-dark"
              >
                Premchand
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to={`/books?searchText=Jane Austen&startIndex=0&maxResults=${max}`}
                className="nav-link text-dark"
              >
                Jane Austen
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/myshelf" className="nav-link text-dark">
                My Books
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/settings" className="nav-link text-dark">
                Settings
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default Navbar;
