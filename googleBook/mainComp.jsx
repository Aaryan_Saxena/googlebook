import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import HomePage from "./homePage";
import ShowBooks from "./showBooks";
import ShowMyBooks from "./showMyBooks";
import Navbar from "./navbar";
import Settings from "./settings";
class MainComp extends Component {
  state = {
    myBooks: [],
    maxResults: "",
    filter: ["printType", "languages", "filter", "orderBy"],
  };
  addBook = (book) => {
    let s1 = { ...this.state };
    s1.myBooks.push(book);
    this.setState(s1);
  };
  removeBook = (book) => {
    let s1 = { ...this.state };
    let index = s1.myBooks.findIndex((bk) => bk.id === book.id);
    s1.myBooks.splice(index, 1);
    this.setState(s1);
  };
  handleMaxResults = (options) => {
    let s1 = { ...this.state };
    s1.maxResults = options.max;
    s1.filter = options.filter;
    this.setState(s1);
  };
  render() {
    return (
      <React.Fragment>
        <Navbar maxResults={this.state.maxResults} />
        <Switch>
          <Route
            path="/books"
            render={(props) => (
              <ShowBooks
                {...props}
                addMyBook={this.addBook}
                removeBook={this.removeBook}
                myBooks={this.state.myBooks}
                max={this.state.maxResults}
                filterArr={this.state.filter}
              />
            )}
          />
          <Route
            path="/myshelf"
            render={(props) => (
              <ShowMyBooks
                {...props}
                removeBook={this.removeBook}
                myBooks={this.state.myBooks}
              />
            )}
          />
          <Route
            path="/settings"
            render={(props) => (
              <Settings
                {...props}
                onOptionChange={this.handleMaxResults}
                filterArr={this.state.filter}
                max={this.state.maxResults ? this.state.maxResults : 8}
              />
            )}
          />
          <Route path="" component={HomePage} />
        </Switch>
      </React.Fragment>
    );
  }
}
export default MainComp;
