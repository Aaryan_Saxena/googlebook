import React, { Component } from "react";
import { Link } from "react-router-dom";
import queryString from "query-string";
import http from "./googleBookServer";
import LeftPanel from "./leftPanel";
class ShowBooks extends Component {
  state = {
    data: {},
    languages: [
      { display: "English", value: "en" },
      { display: "French", value: "fr" },
      { display: "Hindi", value: "hi" },
      { display: "Spanish", value: "es" },
      { display: "Chinese", value: "zh" },
    ],
    orderByOpt: ["newest", "relevance"],
    printTypeOpt: [
      { display: "All", value: "all" },
      { display: "Books", value: "books" },
      { display: "Magazines", value: "magazines" },
    ],
    filterOpt: [
      { display: "Full Volume", value: "full" },
      { display: "Partial Volume", value: "partial" },
      { display: "Free Google e-Books", value: "free-ebooks" },
      { display: "Paid Google e-Books", value: "paid-ebooks" },
    ],
  };
  async fetchdata() {
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    console.log(searchStr, queryParams);
    let index = searchStr.indexOf("=");
    let str = searchStr.substr(index);
    let response = await http.get(`&q=${str}`);
    let { data } = response;
    this.setState({ data: data });
    console.log(response);
  }
  componentDidMount() {
    this.fetchdata();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchdata();
  }
  handlePage = (incr) => {
    let queryParams = queryString.parse(this.props.location.search);
    let { startIndex } = queryParams;
    queryParams.startIndex = +startIndex + incr;
    this.callURL("/books", queryParams);
  };
  handleOptionChange = (options) => {
    console.log(options);
    this.callURL("/books", options);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let {
      searchText,
      startIndex,
      maxResults,
      orderBy,
      filter,
      langRestrict,
      printType,
    } = options;
    console.log(options);
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "searchText", searchText);
    searchStr = this.addToQueryString(searchStr, "startIndex", startIndex);
    searchStr = this.addToQueryString(searchStr, "maxResults", maxResults);
    searchStr = this.addToQueryString(searchStr, "langRestrict", langRestrict);
    searchStr = this.addToQueryString(searchStr, "filter", filter);
    searchStr = this.addToQueryString(searchStr, "printType", printType);
    searchStr = this.addToQueryString(searchStr, "orderBy", orderBy);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue === 0 || paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  render() {
    let { items = [] } = this.state.data;
    let { addMyBook, removeBook, myBooks, filterArr } = this.props;
    const { languages, filterOpt, orderByOpt, printTypeOpt } = this.state;
    let queryParams = queryString.parse(this.props.location.search);
    let endIndex =
      this.state.data.totalItems >= queryParams.maxResults
        ? +queryParams.maxResults === +queryParams.maxResults
          ? +queryParams.startIndex + +queryParams.maxResults
          : +queryParams.startIndex + +queryParams.maxResults
        : this.state.data.totalItems;

    return (
      <div className="container">
        <div className="row">
          <div className="col-3">
            <LeftPanel
              options={queryParams}
              languages={
                filterArr.findIndex((val) => val === "languages") >= 0
                  ? languages
                  : []
              }
              orderByOpt={
                filterArr.findIndex((val) => val === "orderBy") >= 0
                  ? orderByOpt
                  : []
              }
              filterOpt={
                filterArr.findIndex((val) => val === "filter") >= 0
                  ? filterOpt
                  : []
              }
              printTypeOpt={
                filterArr.findIndex((val) => val === "printType") >= 0
                  ? printTypeOpt
                  : []
              }
              onOptionChange={this.handleOptionChange}
            />
          </div>
          <div className="col-9">
            <h4 className="text-center text-warning mt-2">
              {queryParams.searchText} Books
            </h4>
            <span className="text-success">
              {+queryParams.startIndex + 1}-{endIndex} entries
            </span>
            <div className="row">
              {items.map((bk, index) => {
                let {
                  imageLinks = {},
                  title,
                  authors = [],
                  categories = [],
                } = bk.volumeInfo;
                let { smallThumbnail, thumbnail } = imageLinks;
                return (
                  <div
                    key={bk.id}
                    className="col-3 text-center border bg-success"
                  >
                    <img src={smallThumbnail ? smallThumbnail : thumbnail} />{" "}
                    <br />
                    <h5>{title}</h5>
                    <p>
                      {authors.join(" ")}
                      <br />
                      {categories[0]}
                    </p>
                    {myBooks.findIndex((b1) => b1.id === bk.id) >= 0 ? (
                      <button
                        className="btn btn-secondary m-1 btn-sm"
                        onClick={() => removeBook(bk)}
                      >
                        Remove From MyBooks
                      </button>
                    ) : (
                      <button
                        className="btn btn-secondary btn-sm m-1"
                        onClick={() => addMyBook(bk)}
                      >
                        Add to MyBooks
                      </button>
                    )}
                  </div>
                );
              })}
            </div>
            <div className="row">
              <div className="col-2">
                {queryParams.startIndex !== "0" ? (
                  <button
                    className="btn btn-danger m-1 btn-sm shadow"
                    onClick={() => this.handlePage(-+queryParams.maxResults)}
                  >
                    Previous
                  </button>
                ) : (
                  ""
                )}
              </div>
              <div className="col-2 offset-8 text-right">
                {this.state.data.totalItems >= +queryParams.maxResults ? (
                  items.length >= +queryParams.maxResults ? (
                    <button
                      className="btn btn-danger btn-sm m-1 shadow"
                      onClick={() => this.handlePage(+queryParams.maxResults)}
                    >
                      Next
                    </button>
                  ) : (
                    ""
                  )
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowBooks;
